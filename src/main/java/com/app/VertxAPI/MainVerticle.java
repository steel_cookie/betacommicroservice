package com.app.VertxAPI;

import com.app.VertxAPI.Model.Items;
import com.app.VertxAPI.Model.Users;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.jwt.JWTOptions;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.JWTAuthHandler;
import org.mindrot.jbcrypt.BCrypt;

import java.util.List;
import java.util.UUID;

import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;

public class MainVerticle extends AbstractVerticle {

  private static final String PUBLIC_KEY = "catOnKeyboard";
  private static final String PREFIX = "Bearer ";
  private static final int EXPIRATION_TIME = 900;
  private static final String USERS = "users";
  private static final String ITEMS = "items";
  private MongoClient mongo;
  private JWTAuth jwtAuth;

  @Override
  public void start(Future<Void> fut) throws Exception {
    startWebApp(
      (http) -> completeStartUp(http, fut), fut
    );
  }

  private void startWebApp(Handler<AsyncResult<HttpServer>> next, Future<Void> fut) {
    mongo = MongoClient.createShared(vertx, config());
    jwtAuth = getJWTAuth();
    Router router = Router.router(vertx);

    router.route("/*").handler(BodyHandler.create());
    router.post("/login").handler(this::login);
    router.post("/register").handler(this::register);

    router.route("/user/*").handler(JWTAuthHandler.create(jwtAuth));
    router.post("/user/addItem").handler(this::addItem);
    router.get("/user/getItems").handler(this::getItems);

    vertx
      .createHttpServer()
      .requestHandler(router::accept)
      .listen(
        config().getInteger("http.port", 8080),
        next
      );
  }

  private void completeStartUp(AsyncResult<HttpServer> http, Future<Void> fut) {
    if (http.succeeded()) {
      fut.complete();
    } else {
      fut.fail(http.cause());
    }
  }

  private void register(RoutingContext routingContext) {
    JsonObject jsonBody = routingContext.getBodyAsJson();

    Users user = Users.builder()
      .id(UUID.randomUUID())
      .login(jsonBody.getString("login"))
      .password(BCrypt.hashpw(jsonBody.getString("password"), BCrypt.gensalt()))
      .build();

    mongo.insert(USERS, user.toJson(), r ->
      routingContext.response()
        .setStatusCode(OK.code())
        .putHeader(CONTENT_TYPE, "application/jsonBody; charset=utf-8")
        .end("Registering successfull"));
  }

  private void login(RoutingContext routingContext) {
    JsonObject jsonBody = routingContext.getBodyAsJson();
    String login = jsonBody.getString("login");
    String password = jsonBody.getString("password");

    mongo.findOne(USERS, new JsonObject().put("login", login), null, asyncResult -> {
      if (asyncResult.succeeded()) {
        if (asyncResult.result() != null) {
          Users user = new Users(asyncResult.result());
          if (BCrypt.checkpw(password, user.getPassword())) {
            routingContext.response()
              .setStatusCode(OK.code())
              .putHeader(CONTENT_TYPE, "application/jsonBody; charset=utf-8")
              .end(Json.encodePrettily(new JsonObject().put("token", generateJwtToken(login))));
          } else {
            routingContext.response()
              .setStatusCode(BAD_REQUEST.code())
              .end("Incorrect password");
          }
        } else {
          routingContext.response()
            .setStatusCode(BAD_REQUEST.code())
            .end("Incorrect login or password");
        }
      } else {
        routingContext.response()
          .setStatusCode(BAD_REQUEST.code())
          .end("Smth went wrong");
      }
    });
  }

  private void addItem(RoutingContext routingContext) {
    getJsonItem(routingContext, asyncResult -> {
      if (asyncResult.succeeded()) {
        mongo.insert(ITEMS, asyncResult.result(), result -> {
          if (result.succeeded()) {
            routingContext.response()
              .setStatusCode(NO_CONTENT.code())
              .end("Item created successfully");
          } else {
            routingContext.response()
              .setStatusCode(UNAUTHORIZED.code())
              .end("You have not provided an authentication token, the one provided has expired, was revoked or is not authentic");
          }
        });
      }
    });
  }

  private void getItems(RoutingContext routingContext) {
    getAuthorizedUser(routingContext, result -> {
      if (result.succeeded()) {
        mongo.find(ITEMS, new JsonObject().put("owner", result.result().getString("_id")), asyncResult -> {
          if (asyncResult.succeeded()) {
            List<JsonObject> jsonObjects = asyncResult.result();
            jsonObjects.forEach(entries -> entries.remove("owner"));
            routingContext.response()
              .setStatusCode(OK.code())
              .end(Json.encodePrettily(jsonObjects));
          } else {
            routingContext.response()
              .setStatusCode(UNAUTHORIZED.code())
              .end("You have not provided an authentication token, the one provided has expired, was revoked or is not authentic");
          }
        });
      }

    });
  }

  private JWTAuth getJWTAuth() {
    JWTAuthOptions config = new JWTAuthOptions()
      .addPubSecKey(new PubSecKeyOptions()
        .setAlgorithm("HS256")
        .setPublicKey(PUBLIC_KEY)
        .setSymmetric(true));

    return JWTAuth.create(vertx, config);
  }

  private void getJsonItem(RoutingContext routingContext, Handler<AsyncResult<JsonObject>> result) {
    getAuthorizedUser(routingContext, asyncResult -> {
      if (asyncResult.succeeded() && asyncResult.result() != null) {
        Items item = Items.builder()
          .id(UUID.randomUUID())
          .owner(UUID.fromString(asyncResult.result().getString("_id")))
          .name(routingContext.getBodyAsJson().getString("name"))
          .build();
        result.handle(Future.succeededFuture(item.toJson()));
      } else {
        result.handle(Future.failedFuture("Could not get user"));
      }
    });
  }

  private void getAuthorizedUser(RoutingContext routingContext, Handler<AsyncResult<JsonObject>> result) {
    String token = routingContext.request().getHeader("Authorization");
    jwtAuth.authenticate(new JsonObject()
      .put("jwt", token.replace(PREFIX, "")), asyncUser -> {
      if (asyncUser.succeeded()) {
        User authorizedUser = asyncUser.result();
        mongo.findOne(USERS, new JsonObject().put("login", authorizedUser.principal().getValue("login")), null,
          asyncResult -> {
            if (asyncResult.succeeded() && asyncResult.result() != null)
              result.handle(Future.succeededFuture(asyncResult.result()));
            else
              result.handle(Future.failedFuture("Could not get user"));
          });
      }
    });
  }

  private String generateJwtToken(String login) {
    return jwtAuth.generateToken(new JsonObject().put("login", login), new JWTOptions().setExpiresInSeconds(EXPIRATION_TIME));
  }


}
