package com.app.VertxAPI.Model;

import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Items {
  private UUID id;
  private  UUID owner;
  private String name;

  public Items (JsonObject json) {
    this.id = UUID.fromString(json.getString("_id"));
    this.owner = UUID.fromString(json.getString("owner"));
    this.name = json.getString("name");
  }

  public JsonObject toJson () {
    JsonObject json = new JsonObject()
      .put("owner", owner.toString())
      .put("name", name);
    if (id != null) {
      json.put("_id", id.toString());
    }
    return json;
  }

}
