package com.app.VertxAPI.Model;

import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Users {

  private UUID id;
  private String login;
  private String password;

  public Users(JsonObject json) {
    this.id = UUID.fromString(json.getString("_id"));
    this.login = json.getString("login");
    this.password = json.getString("password");
  }

  public JsonObject toJson() {
    JsonObject json = new JsonObject()
      .put("login", login)
      .put("password", password);
    if (id != null) {
      json.put("_id", id.toString());
    }
    return json;
  }


}
